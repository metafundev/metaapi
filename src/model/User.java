package model;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class User {

	private static final int EXPIRATON = 60*24;
	
	private String name;
	private String token;
	private String email;
	private String password;
	private String id;
	private String updatedAt;
	private String createdAt;
	private String sessions;
	private String diskUsage;
	private String diskQuota;
	private String resetPasswordToken;
	private String notifications;
	private String v;
	private String attributes;
	private String home;
	
	private String registryToken;
	
	public User(String _name, String _eMail, String _password)
	{
		this.name = _name;
		this.email = _eMail;
		this.password = _password;
	}
	
	public User()
	{
		
	}
	
	public User (String json)
	{
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(json);
			JSONObject jsonObj = (JSONObject) obj;
			this.name = (String) jsonObj.get("name");
			this.token = (String) jsonObj.get("token");
			this.id = (String) jsonObj.get("id");
			this.updatedAt = "";//(String) jsonObj.get("updatedAt");
			this.createdAt = "";//(String) jsonObj.get("createdAt");
			this.email = (String) jsonObj.get("email");
			this.sessions = "";//(String) jsonObj.get("sessions");
			this.diskUsage ="";// (String) jsonObj.get("diskUsage");
			this.diskQuota ="";// (String) jsonObj.get("diskQuota");
			this.resetPasswordToken = "";//(String) jsonObj.get("resetPasswordToken");
			this.password = (String) jsonObj.get("password");
			this.notifications = "";//(String) jsonObj.get("notifications");
			this.attributes = "";//(String) jsonObj.get("attributes");
			this.home = "";//(String) jsonObj.get("home");
			
		} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private String generateToken()
	{
		return attributes;
		
	}
	
	@Override
	public String toString()
	{
		return (this.name + this.email);
	}
	
	
	
	public String getName() {
		return name;
	}
	public String getToken() {
		return token;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String geteMail() {
		return email;
	}
	public void seteMail(String eMail) {
		this.email = eMail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getSessions() {
		return sessions;
	}

	public void setSessions(String sessions) {
		this.sessions = sessions;
	}

	public String getDiskUsage() {
		return diskUsage;
	}

	public void setDiskUsage(String diskUsage) {
		this.diskUsage = diskUsage;
	}

	public String getDiskQuota() {
		return diskQuota;
	}

	public void setDiskQuota(String diskQuota) {
		this.diskQuota = diskQuota;
	}

	public String getResetPasswordToken() {
		return resetPasswordToken;
	}

	public void setResetPasswordToken(String resetPasswordToken) {
		this.resetPasswordToken = resetPasswordToken;
	}

	public String getNotifications() {
		return notifications;
	}

	public void setNotifications(String notifications) {
		this.notifications = notifications;
	}

	public String getV() {
		return v;
	}

	public void setV(String v) {
		this.v = v;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}
	
}
