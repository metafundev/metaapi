package model;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class FileManagement {
	public String updatedAt;
	public String createdAt;
	public String user;
	public String path;
	public String files;
	public String size;
	public String bioformat;
	public String format;
	public String type;
	public String name;
	public String v;
	
	
	public FileManagement(){}
	
	public FileManagement(String _user, String _path, String _name)
	{
		this.user = _user;
		this.path = _path;
		this.name = _name;
	}
	
	public FileManagement(String _user, String _path, String _name, String _size)
	{
		this.user = _user;
		this.path = _path;
		this.name = _name;
		this.size = _size;
	}
	
	public FileManagement(String _json)
	{
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(_json);
			JSONObject jsonObj = (JSONObject) obj;
			this.name = (String) jsonObj.get("name");
			this.path = (String) jsonObj.get("path");
			this.updatedAt = "";//(String) jsonObj.get("updatedAt");
			this.createdAt = "";//(String) jsonObj.get("createdAt");
			
			
		} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.name;
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getFiles() {
		return files;
	}
	public void setFiles(String files) {
		this.files = files;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getBioformat() {
		return bioformat;
	}
	public void setBioformat(String bioformat) {
		this.bioformat = bioformat;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getV() {
		return v;
	}
	public void setV(String v) {
		this.v = v;
	}
	
}
