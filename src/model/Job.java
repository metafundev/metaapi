package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;

public class Job {

	public String id;
	public String updatedAt;
	public String createdAt;
	public ArrayList<FileManagement> dataFiles;
	public int effectModel;
	public int functional;
	public int organism;
	public String commandLine;
	public String user;
	public String path;
	public String files;
	public String size;
	public String bioformat;
	public String format;
	public String type;
	public String name;
	public int status;
	public String v;
	
	public final static int FIXED_EFFECT_MODEL = 0;
	public final static int RANDOM_EFFECT_MODEL = 1;
	public final static int HIPATHA = 0;
	public final static int GSEA = 1;
	public final static int HOMO_SAPIENS = 0;
	public final static int MUS_MUSCULUS = 1;
	public final static int RATTUS_NORVEGICUS = 2;
	public final static int OTHER_ORGANISM = 3;
	public final static int WORKING = 0;
	public final static int FINISHED =1; 
	public final static int QUEUED = 2;
	public final static int ABORTED = 3;
	
	
	public Job()
	{
		
	}
	
	public Job(String _user, String _name, ArrayList<FileManagement> _dataFiles, int _effectModel, int _functional, int _organism)
	{
		this.user = _user;
		this.name = _name;
		this.dataFiles = _dataFiles;
		this.effectModel = _effectModel;
		this.functional = _functional;
		this.organism = _organism;
		this.createdAt = Calendar.getInstance().getTime().toString();
		this.updatedAt = Calendar.getInstance().getTime().toString();
		this.status = QUEUED;
		//TODO:
		// 		CONSTRUCCTION OF COMMANDLINE TOOL
	
		this.commandLine = "banner Finished";
	}
	
	
	
	
	public String launchJob (String ops)
	{
	    String res = "";
	    String asdf = "qsub Rscript /home/pmalmierca/jobsRPipes/test.r "+ops;
	    Process process = null;
		try {
			this.status = WORKING;
			process = Runtime.getRuntime().exec(asdf);
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}                    
	    BufferedReader reader = new BufferedReader(new InputStreamReader(        
	        process.getInputStream()));                                          
	    String s;                                                                
	    try {
			while ((s = reader.readLine()) != null) {                                
			  System.out.println("Script output: " + s);
			  res += s + "\n";
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    this.status = FINISHED;
	    return res;
	}
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String _id) {
		this.id = _id;
	}
	
	public String getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}


	public String getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	public String getFiles() {
		return files;
	}


	public void setFiles(String files) {
		this.files = files;
	}


	public String getSize() {
		return size;
	}


	public void setSize(String size) {
		this.size = size;
	}


	public String getBioformat() {
		return bioformat;
	}

	public int getStatus() {
		return status;
	}
	
	public void setBioformat(String bioformat) {
		this.bioformat = bioformat;
	}


	public String getFormat() {
		return format;
	}


	public void setFormat(String format) {
		this.format = format;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getV() {
		return v;
	}


	public void setV(String v) {
		this.v = v;
	}
	public ArrayList<FileManagement> getDataFiles() {
		return dataFiles;
	}

	public void setDataFiles(ArrayList<FileManagement> dataFiles) {
		this.dataFiles = dataFiles;
	}

	public int getEffectModel() {
		return effectModel;
	}

	public void setEffectModel(int effectModel) {
		this.effectModel = effectModel;
	}

	public int getFunctional() {
		return functional;
	}

	public void setFunctional(int functional) {
		this.functional = functional;
	}

	public int getOrganism() {
		return organism;
	}

	public void setOrganism(int organism) {
		this.organism = organism;
	}

	public String getCommandLine() {
		return commandLine;
	}

	public void setCommandLine(String commandLine) {
		this.commandLine = commandLine;
	}

}
