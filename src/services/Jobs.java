package services;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.MongoCollection;

import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.Stack;

@Path("jobs")
public class Jobs {
	public final static int WORKING = 0;
	public final static int FINISHED = 1;
	public final static int QUEUED = 2;
	public final static int ABORTED = 3;
	public final static String CORS_ALLOW_URL = "*";
	public static final String PATH_TO_HOMES = "/metafunusers/";
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("saludo")
	public String saludo()
	{
	    return "<html> " + "<title>" + "Hello From Files" + "</title>"
		        + "<body><h1>" + "Hello From Jobs" + "</body></h1>" + "</html> ";
	
	}
	
	@GET
	@Path("/getJobDesign")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getJobDesign(@QueryParam("user")String user, @QueryParam("jobName") String jobName) {
    	String design = "";
    	String aux = "";
    	File resultFile = new File (PATH_TO_HOMES+user+"/results/"+jobName+"/jobDesign.txt");
    	BufferedReader br = null;
		try {
			br = new BufferedReader (new FileReader(resultFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		while ((aux = br.readLine()) != null)
    		{
    			if (aux!=null)
    				design+=aux+"\n";
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(design).build();
		return res;
	}
	
	
	
	@POST
    @Path("/newJob")
    @Consumes(MediaType.TEXT_PLAIN)
	public Response newJob(String args) {
	    String output = "POST:Jersey say : " + args;
	    System.out.println(output);
	    String [] dArgs = args.split(",");
	    String user = dArgs[0].split(":")[1];
	    String jobName = dArgs[1].split(":")[1];
	    String exprFiles = dArgs[2].split(":")[1];
	    String expDesignFiles = dArgs[3].split(":")[1];
	    String designValues = dArgs[4].split(":")[1];
	    String ops = dArgs[5].split(":")[1];
	    System.out.println(user + " " + jobName + " " + exprFiles + " " + expDesignFiles + " " + designValues + " " + ops);
	    this.addJob(user, jobName , exprFiles, expDesignFiles, designValues, ops);
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(output).build();
    	return res;
    }
	
    public String addJob(String user, String name, String files, String expDesignFiles, String designValues, String ops)
	{
		System.out.println(user + " " + name + " " + files + " " + ops);
		Date date = Calendar.getInstance().getTime();
		String [] args = ops.split(" ");
		String effectModel = args[0];
		String functChar = args[1];
		String organism = args[2];
		String cmdLine = "Rscript /metafunR/metafun.R " + ops + " " + files + " " + expDesignFiles + " " + designValues + "/metafunusers/" + user + "/results/" + name;
		
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("jobs");
		Document userDoc = new Document("name", "MongoDB")
                .append("updatedAt", date.toString())
                .append("createdAt", date.toString())
                .append("user", user)
                .append("effectModel", effectModel)
                .append("functional", functChar)
                .append("organism", organism)
				.append("name", name)
				.append("exprFiles", files)
				.append("expDesignFiles", expDesignFiles)
				.append("status", 0)
				.append("commandLine", cmdLine);
		collection.insertOne(userDoc);
		mongoClient.close();
		
		String filePath = PATH_TO_HOMES+user+"/results/" + name;

	    new File (filePath).mkdir();
	    new File (filePath+"/metaRes/GOs").mkdirs();
    	return launchJob(cmdLine, filePath);
	}
	
    public String launchJob (String cmd, String filePath)
	{
	    String res = "";
	    String cmdLine = cmd; // + filePath + " > log.txt";
	    Process process = null;
		System.out.println(cmdLine);
	    try {
			//this.status = WORKING;
			process = Runtime.getRuntime().exec(cmdLine);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}                    
	    BufferedReader reader = new BufferedReader(new InputStreamReader(        
	        process.getInputStream()));                                          
	    String s;                                                                
	    try {
			while ((s = reader.readLine()) != null) {                                
			  System.out.println("Script output: " + s);
			  res += s + "\n";
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    //this.status = FINISHED;
	    return res;
	}
    
    
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("getJobs")
	public String getJobs(@QueryParam("user")String user)
	{
		String jobs = "";
		
		return jobs;
	}
	
    @GET
    @Path("/jobsFromUser")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getResultsFromUser (@QueryParam("user")String user)
    {	
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("jobs");
		String s = "[";
		MongoCursor<Document> cursor = collection.find(eq("user", user)).iterator();
		try {
		    while (cursor.hasNext()) {
		        s+=cursor.next().toJson() + ",";
		    }
		   
		} finally {
			s = s.substring(0, s.length()-1) + "]";
		    cursor.close();
		    mongoClient.close();
		}
		mc.close();
		Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(s).build();
		return res;
    }
    
   
	@GET
	@Path("/getJobStatus")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getJobStatus (@QueryParam ("name") String name)
	{
    	MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");	
		MongoCollection <Document> collection = mdb.getCollection("jobs");
		Document d = collection.find(eq("name",name)).first();
		mc.close();
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(d.toJson());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObj = (JSONObject) obj;
	
		Object status =  jsonObj.get("status");
		Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin",CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(status.toString()).build();
		return res;
	}
	
	@GET
    @Path("/deleteJobsFromUser")
    @Produces(MediaType.TEXT_PLAIN)
    public DeleteResult deleteFilesFromUser (@QueryParam("user")String user)
    {	
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("jobs");
		MongoCursor<Document> cursor = collection.find(eq("user", user)).iterator();
		
		DeleteResult result = collection.deleteMany(eq("user",user));
		try {
			Runtime.getRuntime().exec("rm -rf /metafunusers/"+user+"/results/*");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
    }
	
	
	@GET
	@Path("/deleteJob")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteJob(@QueryParam("user") String user, @QueryParam("jobName") String jobName) 
	{
		Boolean responseEntity = false;
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");	
		MongoCollection <Document> collection = mdb.getCollection("jobs");
		Document d = collection.find(eq("name",jobName)).first();
		collection.deleteOne(d);
		mc.close();
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(d.toJson());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		responseEntity = this.deleteJobFromDisk(user, jobName);
		Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin",CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(responseEntity).build();
		return res;
	}
	
	private boolean deleteJobFromDisk (String user, String jobName) {
		String path = PATH_TO_HOMES + user + "/results/" + jobName;
		File dir = new File(path);
		File[] currList;
		Stack<File> stack = new Stack<File>();
		stack.push(dir);
		while (!stack.isEmpty()) {
		    if (stack.lastElement().isDirectory()) {
		        currList = stack.lastElement().listFiles();
		        if (currList.length > 0) {
		            for (File curr: currList) {
		                stack.push(curr);
		            }
		        } else {
		            stack.pop().delete();
		        }
		    } else {
		        stack.pop().delete();
		    }
		}
		
		return false;
	}
	
	/*
	@GET
	@Path("/downloadJob")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getJobResultPDF(@QueryParam("user") String user, @QueryParam("jobName") String jobName)
	{
		String path = PATH_TO_HOMES+user+"/results/"+jobName;
		byte[] b = null;
        try {
            b = getPDF(path);
            System.out.println(b);
        } catch (IOException e) {
            e.printStackTrace();
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(b, MediaType.APPLICATION_OCTET_STREAM)
                .header("content-disposition", "attachment; filename = " + jobName + ".pdf").build();
		 
	}
	
	private byte[] getPDF(String path) throws IOException
	{
		File f=new File(path);
		ByteArrayOutputStream baos = null;
		ByteArrayInputStream bais = null;
        OutputStream oos = new FileOutputStream(f.getName());

        byte[] buf = new byte[8192];

        InputStream is = new FileInputStream(f);

        int c = 0;

        while ((c = is.read(buf, 0, buf.length)) > 0) {
            oos.write(buf, 0, c);
            //oos.flush();
        }
        
        oos.close();
        //baos = (ByteArrayOutputStream)oos;
        return buf;
	}*/
}
