package services;

import static com.mongodb.client.model.Filters.eq;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.internal.Base64;
import org.bson.types.ObjectId;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.util.JSON;
import com.mongodb.client.MongoCollection;

import model.FileManagement;


@Path("files")
public class Files {
	public static final String PATH_TO_HOMES = "/metafunusers/";
	public static final String CORS_ALLOW_URL = "*";
	public static final String FOREST_DATA = "/home/pmalmierca/jobsRPipes/metafunpipeline/forestdata.r";
	public static final String META_PIPE = "/home/pmalmierca/jobsRpipes/metafunpipeline/metapipe.r";
	public static final int DEL = 5;
	public static final int ADD = 6;
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("saludo")
	public String saludo()
	{
	    return "<html> " + "<title>" + "Hello From Files" + "</title>"
		        + "<body><h1>" + "Hello From Files" + "</body></h1>" + "</html> ";
	
	}
	
	 
	@POST
	@Path("/uploadWithPost")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
		@FormDataParam("file") InputStream uploadedInputStream,
		@FormDataParam("file") FormDataContentDisposition fileDetail,
		@FormDataParam("user") InputStream userInputStream,
		@FormDataParam("user") FormDataContentDisposition userDetail,
		@FormDataParam("type") InputStream typeInputStream,
		@FormDataParam("type") FormDataContentDisposition typeDetail,
		@FormDataParam("path") InputStream pathInputStream,
		@FormDataParam("path") FormDataContentDisposition pathDetail,
		@FormDataParam("parent") InputStream parentInputStream,
		@FormDataParam("parent") FormDataContentDisposition parentDetail,
		@FormDataParam("tech") InputStream techInputStream,
		@FormDataParam("tech") FormDataContentDisposition techDetail) throws IOException
		{
		String fileLocation = "";
        //saving file  
		System.out.println("uploadWithPost before response");
		// PARSING USER
		StringBuilder stringBuilder = new StringBuilder();
		String user = "";
		try {	
			byte[] bytes = new byte[1024]; 
			int read = 0;
			while ((read = userInputStream.read(bytes)) != -1) {
				stringBuilder.append(new String(bytes, 0, read));
			}
		} catch (IOException e) {e.printStackTrace();}
		user = stringBuilder.toString();
		
		// PARSING TECH
		stringBuilder = new StringBuilder();
		String tech = "";
		try {	
			byte[] bytes = new byte[1024]; 
			int read = 0;
			while ((read = techInputStream.read(bytes)) != -1) {
				stringBuilder.append(new String(bytes, 0, read));
			}
		} catch (IOException e) {e.printStackTrace();}
		tech = stringBuilder.toString();
		
		// PARSING PATH
		
		stringBuilder = new StringBuilder();
		String path = "";
		try {	
			byte[] bytes = new byte[1024];
			int read = 0;
			while ((read = pathInputStream.read(bytes)) != -1) {
				stringBuilder.append(new String(bytes, 0, read));
			}
		} catch (IOException e) {e.printStackTrace();}
		path = stringBuilder.toString();
		
		// PARSING PARENT
		
		stringBuilder = new StringBuilder();
		String parent = "";
		try {
			byte[] bytes = new byte[1024];
			int read = 0;
			while ((read = parentInputStream.read(bytes)) != -1) {
				stringBuilder.append(new String(bytes, 0, read));
			}
		} catch (IOException e) {e.printStackTrace();}
		parent = stringBuilder.toString();
		// PARSING TYPE
		
		stringBuilder = new StringBuilder();
		String type = "";
		byte[] bytes = new byte[1024]; 
		int read = 0;
		while ((read = typeInputStream.read(bytes)) != -1) {
			stringBuilder.append(new String(bytes, 0, read));
		}
		type = stringBuilder.toString();
		
		System.out.println(user + " " + type+ " " + tech+ " " + path+ " " + parent);
		
		// FILE SAVING
		int read2 = 0;
		fileLocation = PATH_TO_HOMES + user + "/" + path + "/" + fileDetail.getFileName();
		try {
		    FileOutputStream out = new FileOutputStream(new File(fileLocation));
		    byte[] bytes2 = new byte[1024];
		    out = new FileOutputStream(new File(fileLocation));
		    while ((read2 = uploadedInputStream.read(bytes2)) != -1) {
		        out.write(bytes2, 0, read2);
		    }
		    out.flush();
		    out.close();
		} catch (IOException e) {e.printStackTrace();}
		String fileName = fileDetail.getFileName();
		String bioFormat = "";
				
		if (fileName.substring(fileName.length()-3, fileName.length()).equals("csv"))
			bioFormat = "expressionMatrix";
		if (fileName.substring(fileName.length()-3, fileName.length()).equals("tsv"))
			bioFormat = "experimentDesign";

		Long fileSize = new File(fileLocation).length();
	    this.registerFile(fileLocation, fileDetail.getFileName(), user, fileSize, bioFormat, "FILE", tech, parent, fileName.substring(fileName.length()-3, fileName.length()));

		return Response.status(200)
			      .header("Access-Control-Allow-Origin", "*")
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity("").build();
	}

	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream,
		String uploadedFileLocation) {

		try {
			OutputStream out = new FileOutputStream(new File(
					uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
    @POST
    @Path("/upload")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response uploadFile(String jsonFile) {
	    JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(jsonFile);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObj = (JSONObject) obj;
	    
	    String sEncoded = (String) jsonObj.get("base64");
	    byte[] decoded = Base64.decode(sEncoded);
	    String user = (String) jsonObj.get("user");
	    String fileName = (String) jsonObj.get("fileName");
	    String fileType =(String) jsonObj.get("type");
	    String parentDir = (String) jsonObj.get("parentDir");
	    String dataTech = (String) jsonObj.get("tech");
	    String bioformat = (String) jsonObj.get("bioFormat");	    
	    String filePath = PATH_TO_HOMES + user + "/" + (String) jsonObj.get("path") + fileName;
	    
	    String s = user + " " + fileName + " " + fileType + " " + parentDir + " " + dataTech + " " + bioformat + " " + filePath;

	    try(FileOutputStream fos = new FileOutputStream(filePath)){
	    	fos.write(decoded);
	    }
	    catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    Long fileSize = new File(filePath).length();
	    this.registerFile(filePath, fileName, user, fileSize, bioformat, fileType, dataTech, parentDir, fileName.substring(fileName.length()-3, fileName.length()));
		Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(s).build();
	    System.out.println(s);
	    return res;
    }
    
	public void DeleteFile(String path) {
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");	
		MongoCollection <Document> collection = mdb.getCollection("files");
		Document d = collection.find(eq("path", path)).first();
		JSONParser parser = new JSONParser();
		JSONObject dParsed = null;
		try {
			dParsed = (JSONObject) parser.parse(d.toString());
		} catch (Exception e) {
			System.err.println(e);
		}
		if (dParsed != null) {
			this.updateFolder(dParsed.get("parentDir").toString(), dParsed.get("_id").toString(), dParsed.get("user").toString(), DEL);
		}
		collection.deleteOne(d);
		mc.close();
	}

	private void saveToFile(InputStream uploadedInputStream, String uploadedFileLocation) {

	    try {
	        OutputStream out = null;
	        int read = 0;
	        byte[] bytes = new byte[1024];

	        out = new FileOutputStream(new File(uploadedFileLocation));
	        while ((read = uploadedInputStream.read(bytes)) != -1) {
	            out.write(bytes, 0, read);
	        }
	        out.flush();
	        out.close();
	    } catch (IOException e) {

	        e.printStackTrace();
	    }
	}

    public String getFileID(String user, String path) {
    	String filePath = PATH_TO_HOMES + user + "/" + path;
    	MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");	
		MongoCollection <Document> collection = mdb.getCollection("files");
		Document d = collection.find(eq("path",path)).first();
		mc.close();
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(d.toJson());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jsonObj = (JSONObject) obj;
		String id = jsonObj.get("_id").toString().split(":")[1].replace("}", "").replace("\"", "");

		return id;
    }
    
    @GET
    @Path("/createFolder")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response createFolder(@QueryParam("folderName")String folderName, @QueryParam("user")String user, @QueryParam("parentDir")String parentDir) {	    
    	Response res = null;
	    String filePath = PATH_TO_HOMES+user+"/" +folderName;
	    boolean success = new File (filePath).mkdir();
	    if (success) {
	    	this.registerFolder(folderName, filePath, user, parentDir);
	    	res = Response.status(200)
				      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
				      .header("Access-Control-Allow-Credentials", "true")
				      .header("Access-Control-Allow-Headers",
				        "origin, content-type, accept, authorization")
				      .header("Access-Control-Allow-Methods", 
				        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				      .entity(true).build();
	    	return res;
	    }
		res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(false).build();
	    return res;
	    
    }
    
    @GET
    @Path("/deleteFile")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response deleteFile(@QueryParam("user")String user, @QueryParam("file")String file) {
	    Response res = null;
	    String filePath = file;
	    File f = new File (filePath);
	    this.DeleteFile(filePath);
	    if (f.delete()) {
	    	res = Response.status(200)
				      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
				      .header("Access-Control-Allow-Credentials", "true")
				      .header("Access-Control-Allow-Headers",
				        "origin, content-type, accept, authorization")
				      .header("Access-Control-Allow-Methods", 
				        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				      .entity(true).build();
	    	return res;
	    }
	    res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(false).build();
	    return res;
    }
    
    private Boolean registerFolder(String folderPath, String path, String user, String parentDir) {
    	Date date = Calendar.getInstance().getTime();
		FileManagement fm = new FileManagement(user, path, folderPath);
		String aux [] = folderPath.split("/");
		String folderName = aux[aux.length-1];
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("files");
		Document userDoc = new Document("name", "MongoDB")
                .append("updatedAt", date.toString())
                .append("createdAt", date.toString())
                .append("user", fm.getUser())
                .append("path", fm.getPath())
				.append("name", folderName)
				.append("content", Arrays.asList("5e4e9b335d6d470001bbb750"))
				.append("type", "FOLDER")
				.append("parentDir", parentDir+"/")
				.append("size", "0");
		collection.insertOne(userDoc);
		mongoClient.close();
    	return true;
    }
    
    
    @GET
    @Path("/getContentFolder")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response getFolderContent (@QueryParam("user") String user, @QueryParam("path") String path ) {
    	String files = (String) this.getFilesFromUser(user).getEntity();
    	String respuesta = "";
    	String content = "[";
    	String realPath = PATH_TO_HOMES + user + "/" + path +"/";
    	ArrayList<String> al = new ArrayList<>();
    	JSONParser parser = new JSONParser();
    	Object obj = null;
		try {
			obj = parser.parse(files);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	JSONArray jsonarray = (JSONArray)obj;
    	for (int i = 0; i < jsonarray.size(); i++) {
    		
    	    JSONObject jsonobject = (JSONObject) jsonarray.get(i);
    	    String str = (String) jsonobject.get("path");

    	    if (str.length()>=realPath.length()) {
    	    	if (str.substring(0, realPath.length()).equals(realPath))
    	    		content += jsonobject.toJSONString() + ",";
    	    }
    	}
    	System.out.println(content);
    	if (content.length()>5)
    		respuesta =  content.substring(0,content.length()-1) + "]";
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(respuesta).build();
    	
    	return res;
    }

    private Boolean registerFile(String filePath, String fileName, String user, Long size, String bioFormat, String fileType, String dataTech, String parentDir, String format)
    {
		Date date = Calendar.getInstance().getTime();
		String fileID = "";
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("files");
		Document userDoc = new Document("name", "MongoDB")
                .append("updatedAt", date.toString())
                .append("createdAt", date.toString())
                .append("path", filePath)
                .append("name", fileName)
				.append("user", user)
				.append("size", size)
				.append("bioFormat", bioFormat)
				.append("type", "FILE")
				.append("tech", dataTech) 
				.append("parentDir", parentDir+"/")
				.append("format", format);
		
		collection.insertOne(userDoc);
		mongoClient.close();
		if (!parentDir.equals("/")) {
			fileID = this.getFileID(user, filePath);
			this.updateFolder(parentDir, fileID, user, ADD);
		}
 	   	return true;
    }

    
    private String updateFolder (String folderName, String fileID, String user, int operation) {
    	JSONParser parser = new JSONParser();
    	MongoClient mc = new MongoClient("docker01" , 27017);
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("files");
		BasicDBObject userQuery = new BasicDBObject("user", user);
		BasicDBObject typeQuery = new BasicDBObject("name", folderName);
		BasicDBList and = new BasicDBList();
		and.add(userQuery);
		and.add(typeQuery);
		MongoCursor<Document> cursor = collection.find(Filters.and(userQuery, typeQuery)).iterator();
		String fol = cursor.next().toJson();
		
		JSONObject folder = null;
		try {
			folder = (JSONObject) parser.parse(fol);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String content = folder.get("content").toString();
		content = content.substring(0, content.length()-1);
		content = content + ",\"" + fileID + "\"]";
		Bson newValue = new Document("content", content);
		Bson updateOperationDocument = new Document("$set", newValue);
		
		collection.updateOne(Filters.and(userQuery, typeQuery), updateOperationDocument);
		
		try {
		      File myObj = new File("/metafunusers/log.txt");
		      if (myObj.createNewFile()) {
		        System.out.println("File created: " + myObj.getName());
			      FileWriter myWriter = new FileWriter("/metafunusers/log.txt");
			      myWriter.write(content);
			      myWriter.close();
		      } else {
		        System.out.println("File already exists.");
			      FileWriter myWriter = new FileWriter("/metafunusers/log.txt");
			      myWriter.write(content);
			      myWriter.close();
		      }
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
		
		return content;
    }

    @GET
    @Path("/getJobDesign")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getJobDesign (@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	File jdesign = new File (PATH_TO_HOMES + user + "/results/" + jobName + "/jobDesign.txt");
    	String experimentalDesign = "";
    	BufferedReader br = null;
    	try {
			br = new BufferedReader (new FileReader(jdesign));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		experimentalDesign = br.readLine();
    		

    	} catch(IOException e) {
    		e.printStackTrace();
    	}
    	
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(experimentalDesign).build();
    	return res;
    }
    
    
    
    @GET
    @Path("/checkExperimentalDesign")
    @Produces(MediaType.TEXT_PLAIN)
    public Response checkExperimentalDesign(@QueryParam("user")String user, @QueryParam("csvFile") String csvFile, @QueryParam("ddfFile") String ddfFile) {
    	// String cvsFilePath = PATH_TO_HOMES + user + 
    	File csvF = new File (csvFile);
    	File ddfF = new File (ddfFile);
    	String expermientalDesignMatch = "false";
    	String sCsvSamples = null;
    	ArrayList<String> csvSamples = new ArrayList<>();
    	ArrayList<String> ddfSamples = new ArrayList<>();
    	String aux = null;
    	BufferedReader br = null;
    	try {
			br = new BufferedReader (new FileReader(csvF));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		sCsvSamples = br.readLine();
    		String [] separatedSamples = sCsvSamples.split(",");
    		for (int i = 1; i < separatedSamples.length; i++) {
				csvSamples.add(separatedSamples[i].replaceAll("\"", ""));
			}
    	} catch(IOException e) {
    		e.printStackTrace();
    	}
    	try {
    		br = new BufferedReader(new FileReader(ddfF));
    		while ((aux = br.readLine()) != null)
    		{
    			if (aux!=null)
    			{
    				ddfSamples.add(aux.split("\t")[0]);
    			}
    		}
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	try {
    		for (int i = 0; i < csvSamples.size(); i++) {
				if (csvSamples.get(i).equals(ddfSamples.get(i))) {
					expermientalDesignMatch = "true";
				} else {
					expermientalDesignMatch = "false";
				}
			}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(expermientalDesignMatch).build();
    	return res;
    }
    
    
    @GET
    @Path("/experimentalDesign")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getExperimentalDesign(@QueryParam("user")String user, @QueryParam("filePath")String filePath) {
    	ArrayList<String> arrayDesign = new ArrayList<>();
    	String aux;
    	String design = "";
    	File resultFile = new File (PATH_TO_HOMES+user+"/"+filePath);
    	BufferedReader br = null;
		try {
			br = new BufferedReader (new FileReader(resultFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		while ((aux = br.readLine()) != null)
    		{
    			if (aux!=null)
    			{
    				if (!arrayDesign.contains(aux.split("\t")[1]))
    				{
    					arrayDesign.add(aux.split("\t")[1]);
    				}
    			}
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	for (String s : arrayDesign)
    		design += s + ";";
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(design).build();
    	return res;
    }
    
    
    @GET
    @Path("/getUserFolders")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getUserFolders (@QueryParam("user") String user) {
    	String folders = "[";
    	String items = "";
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("files");
		BasicDBObject userQuery = new BasicDBObject("user", user);
		BasicDBObject typeQuery = new BasicDBObject("type", "FOLDER");
		BasicDBList and = new BasicDBList();
		and.add(userQuery);
		and.add(typeQuery);
		MongoCursor<Document> cursor = collection.find(Filters.and(userQuery, typeQuery)).iterator();
		try {
		    while (cursor.hasNext()) {
		        folders+=cursor.next().toJson() + ",";
		    }

		} finally {
			folders = folders.substring(0, folders.length()-1) + "]";
		    cursor.close();
		    mongoClient.close();
		}
		mc.close();
		JSONParser parser = new JSONParser();
		JSONArray obj = null;
		try {
			obj = (JSONArray)parser.parse(folders);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONArray jsonArr = obj;
		JSONObject jsonObj = null;
		String foldersJSON = "{";
		for (int i = 0 ; i< jsonArr.size(); i++) {
			jsonObj = (JSONObject) jsonArr.get(i);
			System.out.println(jsonObj);
			foldersJSON += "\"" + jsonObj.get("name").toString() + "\":" + this.getFileInfoByID(jsonObj.get("content").toString())+",";
		}
		foldersJSON = foldersJSON.substring(0,foldersJSON.length()-1) + "}";
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(foldersJSON).build();
    	return res;
    }

    @GET
    @Path("/filesFromUser")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getFilesFromUser (@QueryParam("user")String user)
    {	
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("files");
		String s = "[";
		MongoCursor<Document> cursor = collection.find(eq("user", user)).iterator();
		try {
		    while (cursor.hasNext()) {
		        s+=cursor.next().toJson() + ",";
		    }
		   
		} finally {
			s = s.substring(0, s.length()-1) + "]";
		    cursor.close();
		    mongoClient.close();
		}
		mc.close();
		Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(s).build();
		if (s.length()>5) {
			return res;
		}
		return res;
    }
    
    @GET
    @Path("/getFilesFromFolder")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFilesFromFolder(@QueryParam("user") String user, @QueryParam("folderName") String folderName) {
    	MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("files");
		String resData = "Folder Not Found";
		Document d = collection.find(eq("name", folderName)).first();
		String filesIds = "Error";
		ArrayList<String> jsonFiles = new ArrayList<>();
		String [] arrayFiles = null;
		if (d!=null)
		{
			resData = d.toJson();
			JSONParser parser = new JSONParser();
			Object obj = null;
			try {
				obj = parser.parse(d.toJson());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JSONObject jsonObj = (JSONObject) obj;
			filesIds = jsonObj.get("content").toString();
			arrayFiles = filesIds.replaceAll("\"", "").replace("[","").replace("]","").split(",");
			for (String s : arrayFiles) {
				jsonFiles.add(this.getFileInfoByID(s));
			}
		}
		
		mc.close();
		Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(jsonFiles).build();
    	return res;
    }

    private String getFileInfoByID(String idsArr) {
    	MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");
		String [] ids = idsArr.substring(1, idsArr.length()-1).split(",");
		String name = "";
		String res = "[";
		JSONObject jobj = null;
		MongoCollection <Document> collection = mdb.getCollection("files");
		JSONParser jp = new JSONParser();
		for (int i = 0 ; i < ids.length; i++) {
			String f = collection.find(eq("_id", new ObjectId(ids[i].replaceAll("\"", "")))).first().toJson();
			if (f != null) {
				try {
					jobj = (JSONObject) jp.parse(f);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					mc.close();
					e.printStackTrace();
				}
				res += "\"" + jobj.get("name") + "\""; 
				if (i != ids.length-1)
					res+=",";
			}
		}
		res = res.substring(0, res.length()) + "]";
		mc.close();
		return res;
    }
    
    @GET
    @Path("/getResultLines")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getResultLines(@QueryParam("user") String user, @QueryParam("filePath") String filePath, @QueryParam("ini") String ini, @QueryParam("fin") String fin) {
    	File f = new File (filePath);
    	BufferedReader br = null;
    	String aux = null;
    	int iniLine = Integer.parseInt(ini);
    	int finLine = Integer.parseInt(fin);
    	String lines = "";
		try {
			br = new BufferedReader (new FileReader(filePath));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		int i = 0;
    		while ((i < finLine) || ((aux = br.readLine()) != null))
    		{
    			if (i >= iniLine) {
    				lines +=aux;
    			}
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(lines).build();
    	return res;
    }
    
    @GET
    @Path("/getContentResult")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getContentResult (@QueryParam("user")String user, @QueryParam("name")String name) {	
    	String content = "";
    	String aux = "";
    	File resultFile = new File (PATH_TO_HOMES+user+"/results/"+name);
    	BufferedReader br = null;
    	int i = 0;
		try {
			br = new BufferedReader (new FileReader(resultFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		while ((aux = br.readLine()) != null)
    		{
    			if (aux!=null)
    				content+=aux;
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(content).build();
    	return res;	
    }
    
    @GET
    @Path("/getPlotCount")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getPlotCount(@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	int count = 0;
    	File jobFolder = new File(PATH_TO_HOMES+user+"/results/"+jobName);
    	File[] list = jobFolder.listFiles();
    	
    	for(File f : list) {
    		if (f.getName().substring(0, 3).equals("pca"))
    			count++;
    	}
    	
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(""+count).build();
    	
    	return res;
    }
    
    @GET
    @Path("/getStudiesNames")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getStudiesNames(@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	String content = "";
    	String aux = "";
    	File resultFile = new File (PATH_TO_HOMES+user+"/results/"+jobName+"/stdNames.txt");
    	BufferedReader br = null;
    	try {
			br = new BufferedReader (new FileReader(resultFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		while ((aux = br.readLine()) != null)
    		{
    			if (aux!=null)
    				content+=aux+"\n";
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(content).build();
    	return res;
    }
    
    @GET
    @Path("/getBoxplotData")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getBoxplotData(@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	String content = "";
    	String aux = "";
    	File resultFile = new File (PATH_TO_HOMES+user+"/results/"+jobName);
    	BufferedReader br = null;
		try {
			br = new BufferedReader (new FileReader(resultFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		while ((aux = br.readLine()) != null)
    		{
    			if (aux!=null)
    				content+=aux+"\n";
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String resp = this.toBoxplotJSON(content);
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(resp).build();
    	return res;
    }
    
    @GET
    @Path("/getPcaplotData")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getPcaplotData(@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	String content = "";
    	String aux = "";
    	File resultFile = new File (PATH_TO_HOMES+user+"/results/"+jobName);
    	BufferedReader br = null;
		try {
			br = new BufferedReader (new FileReader(resultFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		while ((aux = br.readLine()) != null)
    		{
    			if (aux!=null)
    				content+=aux+"\n";
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String resp = this.toPcaPlotJSON(content);
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(resp).build();
    	return res;
    }
    
    @GET
    @Path("/getDiffExpTopData")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getDifExpTopData(@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	String content = "";
    	String aux = "";
    	File resultFile = new File (PATH_TO_HOMES+user+"/results/"+jobName);
    	BufferedReader br = null;
		try {
			br = new BufferedReader (new FileReader(resultFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		br.readLine();
    		for (int i = 0; i < 20; i++) {
    			aux = br.readLine();
    			if (aux!=null)
    				content+=aux+"\n";
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String resp = this.toDiffExpJSON(content);
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(resp).build();
    	return res;
    }
    
    @GET
    @Path("/getGSEAData")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getGSEAData(@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	String content = "";
    	String aux = "";
    	File resultFile = new File (PATH_TO_HOMES+user+"/results/"+jobName);
    	BufferedReader br = null;
		try {
			br = new BufferedReader (new FileReader(resultFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		br.readLine();
    		while(aux != null) {
    			aux = br.readLine();
    			if (aux!=null)
    				content+=aux+"\n";
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String resp = this.toGSEAJSON(content);
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(resp).build();
    	return res;
    }
    
    @GET
    @Path("/getHipathiaData")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getHipathiaData(@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	String content = "";
    	String aux = "";
    	int count = 0;
    	File resultFile = new File (PATH_TO_HOMES+user+"/results/"+jobName);
    	BufferedReader br = null;
		try {
			br = new BufferedReader (new FileReader(resultFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		br.readLine();
    		while(aux != null && count < 30) {
    			aux = br.readLine();
    			if (aux!=null)
    				content+=aux+"\n";
    			count++;
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String resp = this.toHipathiaJSON(content);
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(resp).build();
    	return res;
    }

    @GET
    @Path("/renameFile")
    @Produces(MediaType.TEXT_PLAIN) 
    public Response renameFile (@QueryParam("user") String user, @QueryParam("oldName") String oldName, @QueryParam("newName") String newName) {
    	String oldPath = PATH_TO_HOMES + user + "/files/" + oldName;
    	String newPath = PATH_TO_HOMES + user + "/files/" + newName;
    	String [] oldsplited = oldName.split("/");
    	String [] newsplited = newName.split("/");
    	String oldDBName = oldsplited[oldsplited.length - 1];
    	String newDBName = newsplited[newsplited.length - 1];
    	String cmdLine = "mv " + oldPath + " " + newPath;
	    Process process = null;
	    try {
			//this.status = WORKING;
			process = Runtime.getRuntime().exec(cmdLine);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("files");
		collection.updateOne(eq("path", oldPath), new Document("$set", new Document("path", newPath).append("name", newDBName)));
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity("").build();
    	mongoClient.close();
        return res;	
    }
    
    
    
    @GET
    @Path("/getMetaData")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMetaData(@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	String content = "";

    	String pathString = PATH_TO_HOMES+user+"/results/"+jobName+"/metaRes/";
    	File jobFolder = new File(pathString);
    	File resultFile = null;
    	File[] list = jobFolder.listFiles();
    	String resp = "";
    	for(File f : list) {
    		if (f.getName().length()>4) {
    			if (f.getName().substring(0, 7).equals("sig.res")) {
        			resultFile = f;
        		}
    		}
    	}
    	if (resultFile == null) {
        	for(File f : list) {
        		if (f.getName().length()>4) {
        			if (f.getName().substring(0, 7).equals("all.res")) {
            			resultFile = f;
            		}
        		}
        	}
    	}
    	if (resultFile != null) {
    		BufferedReader br = null;
    		try {
    			br = new BufferedReader (new FileReader(resultFile));
    		} catch (FileNotFoundException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	try {
        		br.readLine();
        		for (String aux = br.readLine(); aux != null ; aux = br.readLine()) {
        			content+=aux+"\n";
        		}
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	resp = this.toMetaJSON(content);
    	}
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(resp).build();
    	return res;
    }
    
    @GET
    @Path("/getPartialMetaData")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getPartialMetaData(@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	String content = "";

    	String pathString = PATH_TO_HOMES+user+"/results/"+jobName+"/metaRes/";
    	File jobFolder = new File(pathString);
    	File resultFile = null;
    	File[] list = jobFolder.listFiles();
    	String resp = "";
    	for(File f : list) {
    		if (f.getName().length()>4) {
    			if (f.getName().substring(0, 7).equals("sig.res")) {
        			resultFile = f;
        		}
    		}
    	}
    	if (resultFile == null) {
        	for(File f : list) {
        		if (f.getName().length()>4) {
        			if (f.getName().substring(0, 7).equals("all.res")) {
            			resultFile = f;
            		}
        		}
        	}
    	}
    	if (resultFile != null) {
    		BufferedReader br = null;
    		try {
    			br = new BufferedReader (new FileReader(resultFile));
    		} catch (FileNotFoundException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	try {
        		br.readLine();
        		for (int i = 0; i<20 ; i++) {
        			String aux = br.readLine();
        			if (aux != null) {
            			content+=aux+"\n";
        			}
        		}
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	resp = this.toMetaJSON(content);
    	}
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(resp).build();
    	return res;
    }
    
    @GET
    @Path("/getMetaDataGOterm")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMetaResForGo (@QueryParam("user") String user, @QueryParam("jobName") String jobName, @QueryParam("GOTerm") String GOTerm) {
    	String aux = "";
    	String content = "";
    	String pathString = PATH_TO_HOMES+user+"/results/"+jobName+"/metaRes/GOs/"+GOTerm+".txt";
    	String args = PATH_TO_HOMES + user +"/results/"+jobName + " " + GOTerm;
    	File goFile = new File(pathString);
    	if (!goFile.exists())
    		this.generateforestGOData(GOTerm, args);
    	
    	String resp = "";
    	BufferedReader br = null;
		try {
			br = new BufferedReader (new FileReader(goFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
    		while(aux != null) {
    			aux = br.readLine();
    			if (aux!=null)
    				content+=aux;
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(content).build();
    	return res;
    }
    
    private void generateforestGOData (String GOTerm, String args) {
	    String cmdLine = "Rscript " + FOREST_DATA + " " + args;
	    Process process = null;
    	try {
			//this.status = WORKING;
			process = Runtime.getRuntime().exec(cmdLine);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}                    
	    BufferedReader reader = new BufferedReader(new InputStreamReader(        
	        process.getInputStream()));                                          
	    String s;                                                                
	    try {
			while ((s = reader.readLine()) != null) {                                
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
    }
    
    @GET
    @Path ("/getSummaries")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getSummaries (@QueryParam("user") String user, @QueryParam("jobName") String jobName) {
    	String content = "";
    	String aux = "";
    	ArrayList<String> paths = new ArrayList<String>();
    	paths.add(PATH_TO_HOMES + user + "/results/" + jobName + "/summaryDesign.txt");
    	paths.add(PATH_TO_HOMES + user + "/results/" + jobName + "/summaryDifExp.txt");
    	paths.add(PATH_TO_HOMES + user + "/results/" + jobName + "/summaryFunctionalProfiling.txt");
    	paths.add(PATH_TO_HOMES + user + "/results/" + jobName + "/summaryMetaanalysisbp.txt");
    	paths.add(PATH_TO_HOMES + user + "/results/" + jobName + "/summaryMetaanalysismf.txt");
    	paths.add(PATH_TO_HOMES + user + "/results/" + jobName + "/summaryMetaanalysiscc.txt");

    	for (String path : paths){
    		BufferedReader br = null;
    		aux = "";
    		File f = new File(path);
    		if (f.exists()) {
    			try {
        			br = new BufferedReader (new FileReader(path));
        		} catch (FileNotFoundException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
            	try {
            		while(aux != null) {
            			aux = br.readLine();
            			if (aux!=null)
            				content+=aux + "\n";
            		}
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
            	content += "#\n"; 
    		}
    	}

    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(content).build();
    	return res;
    }

    private String toMetaJSON(String content) {
    	String jsonRes = "{\"data\":[";
		String [] items = null;
		String [] lines = content.split("\n");
		for (String s : lines) {
			items = s.split("\t");
			if (items.length > 0) {
				jsonRes += "{\"GOTerm\":\"" + items[0] +"\", \"goName\":\"" + items[1] + "\"," + 
						"\"lor\":" + items[3] + "," +
						"\"pval\":" + items[5] + "," +
						"\"adjPval\":" + items[6] + "," +
						"\"SE\":" + items[9] + "," + 
						"\"confidenceInterval\": \"[" + items[2] + " , " + items[4] + "]\"" +
						"},"; 
			}
		}
		jsonRes = jsonRes.substring(0,jsonRes.length()-1);
		jsonRes += "]}";
		
		return jsonRes;
    }
    
    private String toGSEAJSON(String content) {
    	String jsonRes = "{\"data\":[";
		String [] items = null;
		String [] lines = content.split("\n");
		for (String s : lines) {
			items = s.split("\t");
			jsonRes += "{\"GOTerm\":" + items[7] +", \"N\":" + items[0] + "," +
					"\"lor\":" + items[1] + "," +
					"\"pval\":" + items[2] + "," +
					"\"adjPval\":" + items[3] + "," +
					"\"sd\":" + items[4] + "," +
					"\"t\":" + items[5] + "," +
					"\"GOName\": " + items[8] +
					"},";
		}
		jsonRes = jsonRes.substring(0,jsonRes.length()-1);
		jsonRes += "]}";
		
		return jsonRes;
    }
    
    private String toHipathiaJSON(String content) {
    	String jsonRes = "{\"data\":[";
		String [] items = null;
		String [] lines = content.split("\n");
		for (String s : lines) {
			items = s.split("\t");
			jsonRes += "{\"pathway\":" + items[0] +", \"name\":" + items[1] + "," + 
					"\"upDown\":" + items[2] + "," +
					"\"statistic\":" + items[3] + "," +
					"\"pval\":" + items[4] + "," +
					"\"adjpval\":" + items[5] +
					"},"; 
		}
		jsonRes = jsonRes.substring(0,jsonRes.length()-1);
		jsonRes += "]}";
		
		return jsonRes;
    }
    
    private String toDiffExpJSON(String content) {
    	String jsonRes = "{\"data\":[";
		String [] items = null;
		String [] lines = content.split("\n");
		for (String s : lines) {
			items = s.split("\t");
			jsonRes += "{\"Entrez\":" + items[6]+", \"logFc\":" + items[0] + "," + 
					"\"AveExpr\":" + items[1] + "," +
					"\"t\":" + items[2] + "," +
					"\"Pvalue\":" + items[3] + "," +
					"\"adjPvalue\":" + items[4] + "," +
					"\"GeneName\":" + items[7] + "," +
					"\"B\": " + items[5] + 
					"},";
		}
		jsonRes = jsonRes.substring(0,jsonRes.length()-1);
		jsonRes += "]}";
		
		return jsonRes;
    }
    
    private String toPcaPlotJSON(String content) {
    	String jsonRes = "{\"data\":[";
		String [] items = null;
		String [] lines = content.split("\n");
		for (String s : lines) {
			items = s.split("\t");
			jsonRes += "{\"sample\":\"" + items[0]+"\", \"design\":" + "\"" + items[4] + "\"," + 
					"\"PC1\":" + items[1] + "," +
					"\"PC2\": " + items[2] + "," +
					"\"PC3\": " + items[3] + "," +
					"\"varExp\": \"" + items[5] + "\"" +
					"},"; 
		}
		jsonRes = jsonRes.substring(0,jsonRes.length()-1);
		jsonRes += "]}";
		return jsonRes;
	}

	@GET
    @Path("/getImage")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getAnalysis (@QueryParam("user")String user, @QueryParam("imgPath") String imgPath) throws Exception {
		String realPath = PATH_TO_HOMES + user + "/results/" + imgPath;        
        File file = new File(realPath);

        return Response.ok(file, "image/jpg")
        		.header("Inline", "filename=\"" + file.getName() + "\"")
        		.header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			    .header("Access-Control-Allow-Credentials", "true")
			    .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			    .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .build();
    }
    
    private String encodeFileToBase64Binary(String fileName) throws IOException {

		File file = new File(fileName);
		byte[] bytes = loadFile(file);
		byte[] encoded = java.util.Base64.getEncoder().encode(bytes);
		String encodedString = new String(encoded);

		return encodedString;
	}

	@GET
	@Path ("/downloadFile")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadFile (@QueryParam("user") String user, @QueryParam("filePath") String filePath) {
		String realPath = PATH_TO_HOMES + user + "/results/" + filePath;
        File file = new File(realPath);
		byte[] bytes = null;
		try {
			bytes = loadFile(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(bytes).build();
        return res;
	}
	
	@GET
	@Path ("/downloadAny")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadAny (@QueryParam("user") String user, @QueryParam("filePath") String filePath) {
		String realPath = PATH_TO_HOMES + user + filePath;
        File file = new File(realPath);
		byte[] bytes = null;
		try {
			bytes = loadFile(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(bytes).build();
        return res;
	}
	
	private static byte[] loadFile(File file) throws IOException {
	    InputStream is = new FileInputStream(file);

	    long length = file.length();
	    if (length > Integer.MAX_VALUE) {
	        // File is too large
	    }
	    byte[] bytes = new byte[(int)length];
	    
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length
	           && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }

	    if (offset < bytes.length) {
	    	is.close();
	        throw new IOException("Could not completely read file " + file.getName());
	    }

	    is.close();
	    return bytes;
	}
	
	@GET
    @Path("/getDiskUsage")
    @Produces(MediaType.TEXT_PLAIN)
	public Response getDiskUsage(@QueryParam("user") String user) {
	    long size = this.getFolderSize(new File(PATH_TO_HOMES + user));
	    String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
	    int unitIndex = (int) (Math.log10(size) / 3);
	    double unitValue = 1 << (unitIndex * 10);
	    String readableSize = new DecimalFormat("#,##0.#")
	                                .format(size / unitValue) + " "
	                                + units[unitIndex];
    	Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(readableSize).build();
		return res;
	}
	
	
	
	
	
	
	private long getFolderSize(File folder) {
	    long length = 0;
	    File[] files = folder.listFiles();
	    int count = files.length;
	    for (int i = 0; i < count-1; i++) {
	        if (files[i].isFile()) {
	            length += files[i].length();
	        }
	        else {
	            length += getFolderSize(files[i]);
	        }
	    }
	    return length;
	}
	
	private String toBoxplotJSON (String content) {
		String jsonRes = "{\"data\":[";
		String [] items = null;
		String [] lines = content.split("\n");
		for (String s : lines) {
			items = s.split("\t");
			jsonRes += "{\"sample\":\"" + items[0]+"\", \"design\":" + "\"" + items[6] + "\"," + 
					"\"values\": { \"Q1\":" + items[2] + "," +
					"\"Q2\": " + items[3] + "," +
					"\"Q3\": " + items[4] + "," +
					"\"min\": " + items[1] + "," +
					"\"max\": " + items[5] + "," +
					"\"outliers\": []}" + 
					"},"; 
		}
		jsonRes = jsonRes.substring(0,jsonRes.length()-1);
		jsonRes += "]}";
		
		return jsonRes;
	}
	
	@GET
    @Path("/deleteFilesFromUser")
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteFilesFromUser (@QueryParam("user")String user)
    {	
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("files");
		
		DeleteResult result = collection.deleteMany(eq("user",user));
		try {
			Runtime.getRuntime().exec("rm -rf /metafunusers/"+user+"/files/*");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mongoClient.close();
		return result.toString();
    }
	
	@GET
	@Path("/deleteFolder")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteFolder (@QueryParam("user")String user, @QueryParam("folderPath")String folderPath) {
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("files");
		Pattern folderPattern = Pattern.compile(folderPath + "/*");
		Bson query = new BasicDBObject("user", user)
			    .append("path", folderPattern);
		try {
			Runtime.getRuntime().exec("rm -rf " + folderPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		DeleteResult result = collection.deleteMany(query);

		
		mongoClient.close();
		return result.toString() + folderPath;
	}

}
