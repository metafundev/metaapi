package services;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.MongoCollection;

import model.User;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

@Path("users")
public class Users {
	public static final String PATH_TO_HOMES = "/metafunusers/";
	public static final String CORS_ALLOW_URL = "*";
	String name;
	String eMail;

	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("saludo")
	public String saludo()
	{
	    return "<html> " + "<title>" + "Hello From Users" + "</title>"
		        + "<body><h1>" + "Hello From Users" + "</body></h1>" + "</html> ";
	
	}
		
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("firstUser")
	public String firstUser()
	{
		MongoClient mc = new MongoClient("docker01" , 27017);
		@SuppressWarnings("deprecation")
		DB db = mc.getDB("stevia-server");
		DBCollection coll = db.getCollection("users");
			
		DBObject myDoc = coll.findOne();
		mc.close();
		return myDoc.toString();
	}
		
	private String getUser(@QueryParam(value="eMail")String eMail)
	{
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");	
		MongoCollection <Document> collection = mdb.getCollection("users");
		Document d = collection.find(eq("email",eMail)).first();
		mc.close();
		System.out.println(d);
		if (d!=null)
			return d.toJson();
		else
			return null;
	}
	
	private String getUserByToken (String token)
	{
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");	
		MongoCollection <Document> collection = mdb.getCollection("users");
		Document d = collection.find(eq("token",token)).first();
		mc.close();
		System.out.println(d);
		if (d!=null)
			return d.toJson();
		else
			return null;
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("getUserName")
	public Response getUserName(@QueryParam(value="eMail")String eMail)
	{
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");	
		MongoCollection <Document> collection = mdb.getCollection("users");
		Document d = collection.find(eq("email",eMail)).first();
		mc.close();
		User u = new User(d.toJson());
		Response res = Response.status(200)
	      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
	      .header("Access-Control-Allow-Credentials", "true")
	      .header("Access-Control-Allow-Headers",
	        "origin, content-type, accept, authorization")
	      .header("Access-Control-Allow-Methods", 
	        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	      .entity(u.getName())
	      .build();
		return res;
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("getUserToken")
	public Response getUserToken(@QueryParam(value="eMail")String eMail)
	{
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");	
		MongoCollection <Document> collection = mdb.getCollection("users");
		Document d = collection.find(eq("email",eMail)).first();
		mc.close();
		User u = new User(d.toJson());
		Response res = Response.status(200)
	      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
	      .header("Access-Control-Allow-Credentials", "true")
	      .header("Access-Control-Allow-Headers",
	        "origin, content-type, accept, authorization")
	      .header("Access-Control-Allow-Methods", 
	        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	      .entity(u.getToken())
	      .build();
		return res;
	}
	
	public String getHash(String txt, String hashType) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest
                    .getInstance(hashType);
            byte[] array = md.digest(txt.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
 
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("getAllUsers")
	public Response getAllUsers()
	{
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("users");
		String s = "";
		MongoCursor<Document> cursor = collection.find().iterator();
		try {
		    while (cursor.hasNext()) {
		        s+=cursor.next().toJson();
		    }
		   
		} finally {
		    cursor.close();
		    mongoClient.close();
		}
		mc.close();
		Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(s).build();
		return res;
	}
		
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("signUp")
	public Response signUp(@QueryParam(value="name")String name, @QueryParam(value="eMail")String eMail, @QueryParam(value="password")String password)
	{
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("users");
		
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("name", name);
		MongoCursor<Document> cursor = collection.find(searchQuery).iterator();
		if (cursor.hasNext()) {
			Response res = Response.status(200)
				      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
				      .header("Access-Control-Allow-Credentials", "true")
				      .header("Access-Control-Allow-Headers",
				        "origin, content-type, accept, authorization")
				      .header("Access-Control-Allow-Methods", 
				        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				      .entity("false").build();
			return res;
		}
		
		
		Date date = Calendar.getInstance().getTime();
		User u = new User(name, eMail, password);
		String token = this.getHash(name+date.toString(), "MD5");
		Document userDoc = new Document("name", "MongoDB")
				.append("type", "database")
				.append("count", 1)
                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
                .append("updatedAt", date.toString())
                .append("createdAt", date.toString())
                .append("name", u.getName())
                .append("token", token)
                .append("email", u.geteMail())
				.append("password", u.getPassword());
		collection.insertOne(userDoc);
		mongoClient.close();
		this.createUser(name);
		try {
			Runtime.getRuntime().exec("cp -r /metafunR/files_examples/ /metafunusers/" + name + "/files/");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Response res = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity("true").build();
		return res;
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("logIn")
	public Response logIn(@QueryParam(value="eMail")String eMail,@QueryParam(value="password")String password)
	{
		String uJSON = this.getUser(eMail);
		String dbPass = "";
		String dbEMail = "";
		JSONParser parser = new JSONParser();
		Boolean logInIsCorrect = false;
		if (uJSON != null) {
			try {
				Object obj = parser.parse(uJSON);
				JSONObject jsonObj = (JSONObject) obj;
				
				dbPass = (String) jsonObj.get("password");
				dbEMail = (String) jsonObj.get("email");
				if (dbPass.equals(password) && dbEMail.equals(eMail))
				{
					logInIsCorrect = true;
				}
				else 
				{
					logInIsCorrect = false;
				}
				
			} catch (org.json.simple.parser.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
		Response resp = Response.status(200)
			      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
			      .header("Access-Control-Allow-Credentials", "true")
			      .header("Access-Control-Allow-Headers",
			        "origin, content-type, accept, authorization")
			      .header("Access-Control-Allow-Methods", 
			        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
			      .entity(logInIsCorrect)
			      .build();
		return resp;
	}
	
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("deleteUser")
	public Response deleteUser (@QueryParam(value="user") String token) {
		MongoClient mc = new MongoClient("docker01" , 27017);
		MongoDatabase mdb = mc.getDatabase("stevia-server");	
		MongoCollection <Document> collection = mdb.getCollection("users");
		MongoCollection <Document> jCollection = mdb.getCollection("jobs");
		MongoCollection <Document> fCollection = mdb.getCollection("files");

		Document d = collection.find(eq("token",token)).first();
		User u = new User(d.toJson());
		DeleteResult res = collection.deleteOne(d);
		String name = u.getName();
		Bson query = eq("user", name);
		try {
            DeleteResult result = jCollection.deleteMany(query);
            DeleteResult resul2t = fCollection.deleteMany(query);
            System.out.println("Deleted document count: " + result.getDeletedCount());
        } catch (MongoException me) {
            System.err.println("Unable to delete due to an error: " + me);
        }
		
		mc.close();
			
		try {
			Runtime.getRuntime().exec("rm -rf /metafunusers/"+u.getName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Response resp = Response.status(200)
	      .header("Access-Control-Allow-Origin", CORS_ALLOW_URL)
	      .header("Access-Control-Allow-Credentials", "true")
	      .header("Access-Control-Allow-Headers",
	        "origin, content-type, accept, authorization")
	      .header("Access-Control-Allow-Methods", 
	        "GET, POST, PUT, DELETE, OPTIONS, HEAD")
	      .entity(u.getName())
	      .build();
		return resp;
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("changePassword")
	public Boolean changePassword(@QueryParam(value="eMail")String eMail,@QueryParam(value="password")String password, @QueryParam(value="newPassword")String newPassword) {
		Date date = Calendar.getInstance().getTime();
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("users");
		String uJSON = this.getUser(eMail);
		User u = new User(uJSON);
		String dbPass = "";
		String dbEMail = "";
		Document query = new Document();
		query.append("email", eMail);
		Document setData = new Document();
		setData.append("password", newPassword);
		JSONParser parser = new JSONParser();
		Boolean changeIsCorrect = false;
		try {
			JSONObject jsonObj = (JSONObject)parser.parse(uJSON);
			
			dbPass = (String) jsonObj.get("password");
			dbEMail = (String) jsonObj.get("email");

			if (dbPass.equals(password) && dbEMail.equals(eMail))
			{
				Document update = new Document();
				update.append("$set", setData);
				collection.updateOne(query, update);
				changeIsCorrect = true;
			}
			else 
			{
				changeIsCorrect = false;
			}
			
		} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			mongoClient.close();
			e.printStackTrace();
		}
		mongoClient.close();
		return (changeIsCorrect);
		
	}
		
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("resetPassword")
	public Boolean resetPassword(@QueryParam(value="eMail")String eMail) {
		Date date = Calendar.getInstance().getTime();
		MongoClientURI connString = new MongoClientURI("mongodb://docker01:27017");
		MongoClient mongoClient = new MongoClient(connString);
		MongoDatabase mdb = mongoClient.getDatabase("stevia-server");
		MongoCollection <Document> collection = mdb.getCollection("users");
		String uJSON = this.getUser(eMail);
		User u = new User(uJSON);
		String dbPass = "";
		String dbEMail = "";
		Document query = new Document();
		query.append("email", eMail);
		Document setData = new Document();
		setData.append("password", generatePassword());
		JSONParser parser = new JSONParser();
		Boolean changeIsCorrect = false;
		try {
			JSONObject jsonObj = (JSONObject)parser.parse(uJSON);
			dbPass = (String) jsonObj.get("password");
			dbEMail = (String) jsonObj.get("email");

			if (dbEMail.equals(eMail))
			{
				Document update = new Document();
				update.append("$set", setData);
				collection.updateOne(query, update);
				changeIsCorrect = true;
			}
			else 
			{
				changeIsCorrect = false;
			}
			
		} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			mongoClient.close();
			e.printStackTrace();
		}
		mongoClient.close();
		return (changeIsCorrect);
		
	}
	
	private String generatePassword()
	{
		int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 16;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    String generatedString = buffer.toString();
	 
	    return generatedString;
	}
	
	private Boolean createUser(String name)
	{
		File dir = new File (PATH_TO_HOMES+name);
		dir.mkdir();
		dir = new File (PATH_TO_HOMES+name+"/files");
		dir.mkdir();
		dir = new File (PATH_TO_HOMES+name+"/results");
		dir.mkdir();
		return false;
	}
	
}
